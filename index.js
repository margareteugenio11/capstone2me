const express = require("express");
const app = express();
const mongoose = require("mongoose")
const userRoute = require("./routes/user")
const authRoute = require("./routes/auth")

mongoose.connect("mongodb+srv://admin:admin131@cluster0.ycdbp.mongodb.net/Capstone2?retryWrites=true&w=majority")
.then(() => console.log("DBConnection Successfull!"))
.catch((err) => {console.log(err);

});
	app.use(express.json());
app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);

app.listen(process.env.PORT || 4000, () => {
	console.log("Backend server is running");
});
